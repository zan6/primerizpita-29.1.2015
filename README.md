V podjetju, kjer ste se zaposlili kot razvijalec ste dobili svojo prvo nalogo in sicer morate razviti del funkcionalnosti obstoječe spletne aplikacije.
Vaša naloga je implementirati naslednja 2 primera uporabe: Prijava uporabnika v sistem in Zunanja avtentikacija, ki sta prikazana na spodnjem diagramu primerov uporabe.
![1.png](https://bitbucket.org/repo/yaXEob/images/2072648795-1.png)

PU Prijava uporabnika v sistem mora podpirati naslednjo funkcionalnost:
1.	uporabnik obišče spletno stran in prikaže se mu vnosni obrazec, kjer mora vnesti zahtevane podatke,

2.	uporabnik vnese uporabniško ime in geslo ter pritisne gumb Prijava,

3.	pred pošiljanjem podatkov na strežnik, se uporabniško ime in geslo najprej preverita na strani odjemalca,
       *pri preverjanju uporabniških imen in gesel morate uporabiti 2 vira:
	      *spremenljivko podatkiSpomin, kjer so prednastavljeni določeni uporabniki, ki imajo pravico za dostop in
	      *datoteko public/podatki/uporabniki_odjemalec.json, kjer je na voljo seznam uporabnikov, ki imajo ravno tako dostop do sistema,
4.	če je bilo preverjanje na odjemalcu uspešno, se zahteva posreduje strežniku, ki uporabniško ime in geslo ponovno preveri,
	*tudi na strežniku se pri preverjanju uporabniških imen in gesel morata uporabiti 2 vira:
	       *spremenljivko podatkiSpomin, kjer so prednastavljeni določeni uporabniki, ki imajo pravico za dostop in
	       *datoteko public/podatki/uporabniki_streznik.json, kjer je na voljo seznam uporabnikov, ki imajo ravno tako dostop do sistema,
5.	če preverjanje na odjemalcu ni bilo uspešno, se izpiše napaka (v element DIV, z ID-jem "status") in sicer:
    *"Prosim vnesite uporabniško ime in geslo.", če uporabnik ni vnesel ne uporabniškega imena, ne gesla,
    *"Prosim vnesite geslo.", če uporabnik vnese samo uporabniško ime, gesla pa ne,
    *"Prosim vnesite uporabniško ime.", če uporabnik vnese samo geslo, uporabniškega imena pa ne,
    *"Uporabniško ime in/ali geslo je napačno.", če je vnesel vse podatke in je uporabniško ime in/ali geslo napačno.
6. če se uporabniško ime in geslo ujemata s podatki v sistemu, je uporabniku dovoljena prijava, sicer se izpiše napaka:
*npr. za uporabnika "dejan", je rezultat naslednji:
*"<p>Uporabnik <b>dejan</b> uspešno prijavljen v sistem!</p>", če je prijava na strežniku uspela,
*"<p>Uporabnik <b>dejan</b> nima pravice prijave v sistem!</p>", če prijava na strežniku ni uspela,
*prav tako je potrebno nastaviti tudi naslov strani (title), ki se prikaže uporabniku in sicer:
*"<title>Uspešno</title>", če je prijava na strežniku uspela in
*"<title>Napaka</title>", če prijava na strežniku ni uspela.
Spodnja slika prikazuje podrobnosti delovanja prijave uporabnika v sistem v obliki diagrama interakcij:
![2.png](https://bitbucket.org/repo/yaXEob/images/2945336126-2.png)
PU Zunanja avtentikacija mora podpirati naslednjo funkcionalnost:
1. zunanji sistem lahko zahteva avtentikacijo preko API-ja, ki ga naš sistem implementira,
    o zahteva je v obliki /api/prijava?uporabniskoIme=dejan&geslo=test,
2. strežnik preveri ali se uporabniško ime in geslo ujemata s podatki v sistemu in odjemalcu posreduje odgovor.
opodobno kot v prejšnjem primeru je potrebno uporabniško ime in geslo preveriti v 2 virih (iz spremenljivke v spominu in iz datoteke uporabniki_streznik.json),
orezultat se odjemalcu posreduje v naslednji obliki JSON objekta:
   *{status: "status", napaka: "napaka"},
   *kjer je vrednost spremenljivke "status" lahko true (če je avtentikacija uspela) oz. false (če ni uspela),
	*vrednost spremenljivke napaka pa je lahko:
	*"" (t.j. prazna), če je avtentikacija uspela,
	*"Napačna zahteva.", če je uporabniško ime in/ali geslo prazno ali je zahteva napačna,
	*"Avtentikacija ni uspela.", če se podani uporabniško ime in geslo ne ujemata s podatki v sistemu.
Spodnja slika prikazuje podrobnosti delovanja zunanje avtentikacije v obliki diagrama interakcij:
![3.png](https://bitbucket.org/repo/yaXEob/images/658285705-3.png)
Razredni diagram, ki prikazuje zasnovo celotne rešitve je prikazan na spodnji sliki:
![4.png](https://bitbucket.org/repo/yaXEob/images/4276950725-4.png)
Namigi:

*vse spremembe, ki jih morate implementirati izvedete v datotekah public/index.html in streznik.js,

*za razhroščevanje na strani odjemalca uporabljajte Chrome JavaScript konzolo,

*za branje datoteke na strani strežnika lahko uporabite metodo fs.readFileSync(...).toString(),

*za pretvorba niza v JSON obliko lahko uporabite metodo JSON.parse(...),

*za branje datoteke na strani odjemalca lahko uporabite metodo $.getJSON(...),

*mapa s podatki na strežniku se nahaja na __dirname + "/public/podatki/", kar vam bo olajšalo dostop do datoteke na strežniškem delu,

*imena atributov, ki jih posredujete iz obrazca na novo stran so enaka atributom name pri vnosnih poljih (npr. uporabniskoIme in geslo).